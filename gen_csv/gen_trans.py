# Simple script to export localization file from a csv. 
# The 3rd column will be exported with a filename same as the header (first row)
import csv

# TODO: set the following with cli parameters
transCol = 2
transFile = 'Greek.txt'
csvSep = '|'
inSep = '|$|'
csvFile = 'translations.csv'
debugOut = True

# Read csv file
with open(csvFile) as f:
    csvReader = csv.reader(f,delimiter=csvSep)
    lines = [f'{line[0]}={line[transCol]}\n' for line in csvReader if len(line) >=3]
if(debugOut):
    print(lines)

del lines[0] #first line contains headers

# Write translation file
# Write to csv file
fout = open(transFile,'w')
fout.writelines(lines)
fout.close()