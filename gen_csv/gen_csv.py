# Simple script to combine 2 localization files to one csv file
# The first file (usually english) will be used as a base for available localization keys

# TODO: set the following with cli parameters
baseFile = 'English.txt'
prevBaseFile = 'English_old.txt'
transFile = 'Greek.txt'
csvSep = '|'
inSep = '|$|'
csvFile = 'translations.csv'
debugOut = True

# Read base file and create a key,text dictionary
with open(baseFile) as f:
    engLines = [line.rstrip().replace('=',inSep,1) for line in f]
engLines = {
    line.split(inSep)[0] : line.split(inSep)[1] 
    for line in engLines 
    if len(line.split(inSep)) == 2
}
if(debugOut):
    print(engLines)

# Read previous base file and create a key,text dictionary
with open(prevBaseFile) as f:
    prevEngLines = [line.rstrip().replace('=',inSep,1) for line in f]
prevEngLines = {
    line.split(inSep)[0] : line.split(inSep)[1] 
    for line in prevEngLines 
    if len(line.split(inSep)) == 2
}
if(debugOut):
    print(prevEngLines) 

# Read translation file and create a key,text dictionary
with open(transFile) as f:
    grLines = [line.rstrip().replace('=',inSep,1) for line in f]
grLines = {
    line.split(inSep)[0] : line.split(inSep)[1] 
    for line in grLines 
    if len(line.split(inSep)) == 2
}
if(debugOut):
    print(grLines)

# Combine 2 dictionaries to a list of text lines to generate in csv file
final = [
    f'"{key}"{csvSep}"{eng}"{csvSep}"{grLines[key] if key in grLines else ""}"{csvSep}{f"NEW{csvSep}" if key not in prevEngLines else f"CHANGED{csvSep}{prevEngLines[key]}" if prevEngLines[key]!=engLines[key] else f"MISSING{csvSep}" if eng!="" and (key not in grLines or grLines[key]=="") else f"OK{csvSep}"}\n'
    for key,eng in engLines.items()    
]
# add headers
final.insert(0,f'Key{csvSep}English{csvSep}Greek{csvSep}Status{csvSep}Info (old Eng)\n')
if(debugOut):
    print(final)

# Write to csv file
fout = open(csvFile,'w')
fout.writelines(final)
fout.close()
